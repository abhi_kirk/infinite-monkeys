from dash.dependencies import Input, Output
from app import app

from utils.backend.etl import ETL
from utils.backend.asys import Asys
from utils.backend.model import Model
from utils.backend.init import INIT

import plotly.graph_objects as go
import plotly.express as px

import pandas as pd
import os
import dash_html_components as html
from matplotlib import pyplot as plt
import matplotlib
matplotlib.use('Agg')   # Backend that doesn't display plt to the user


@app.callback(Output('header', 'children'),
              Input('interval-component', 'n_intervals'))
def update_dbtext(_):
    default_text = ["Analysis of market behavior "
                    "in NYSE and Nasdaq and stock value prediction "
                    "based on data-driven modeling",
                    html.P()]
    hbeat_path = os.path.join("appdata", "hbeat.txt")
    db_text = ["DynamoDB Update App: N/A"]
    if os.path.isdir("appdata") and os.path.isfile(hbeat_path):
        f = open(hbeat_path, 'r')
        f_lines = [i.replace('\n', '') for i in f.readlines()]
        f_lines_active = [i for i in f_lines if 'ACTIVE' in i]
        if f_lines_active:
            try:
                db_text = [db_text[0].replace('N/A', 'ACTIVE ')]
                db_text += [html.Br(), "Last Updated: ", f_lines_active[-1].split('--')[0]]
            except:
                pass
    return default_text + db_text


@app.callback(
    [Output("price-chart", "figure"), Output("diagnostic-chart", "figure")],
    [
        Input("ticker-filter", "value"),
        Input("date-range", "start_date"),
        Input("date-range", "end_date"),
    ],
)
def update_charts(ticker, start_date, end_date):

    # # --- INITIALIZE ---
    config = INIT('utils/backend/config.json')
    config.read_json()
    param_filter = config.config_file_data['filter_param']
    param_fig = config.config_file_data['fig_sizes']
    param_model = config.config_file_data['analysis_model_param']

    stock = ETL(ticker, config.config_file_data['dynamo_db'][0])
    stock.stock_data(start_date, end_date)

    # If pre-IPO date chosen by user, revert to final date of today - 1 to ensure data pull
    # TODO: In future, add a display panel on the dash and display warning
    #  when ETL returns no data

    data_update = stock.old

    # # --- ANALYSIS ---
    # SavGol filtering
    stock_analysis = Asys()
    stock_analysis.filter(data_update.Value, param_filter[0]["win_size"], param_filter[0]["poly_order"])
    stock_model = Model()

    # [Optional Plot] filtered values
    # stock_analysis.plot_raw_filtered(curr_stock.old.High,param_fig[0]["width"],
    #                                  param_fig[0]["height"], curr_stock.name, ' Daily High Value')

    # [Optional Plot] Moving mean and STD
    # Check for stationarity of timeseries
    # stock_analysis.check_stationarity(curr_stock.old.High, param_model[0]["moving_win_size"],
    #                                   param_fig[0]["width"], param_fig[0]["height"],
    #                                   param_fig[0]["width_larger_plots"], param_fig[0]["height_larger_plots"],
    #                                   curr_stock.name, ' Daily High Value')

    # # --- MODELING ---
    # ARIMA timeseries model
    stock_model.model_arima(stock_analysis.filtered,
                            param_model[0]["p_start"],
                            param_model[0]["q_start"],
                            param_model[0]["p_max"],
                            param_model[0]["q_max"],
                            param_model[0]["m"],
                            param_model[0]["data_split_train_perc"],
                            param_fig[0]["width_larger_plots"],
                            param_fig[0]["height_larger_plots"])

    # Forecast future values
    stock_model.forecast_val(param_model[0]["alpha"],
                             param_model[0]["dpi"],
                             param_fig[0]["width"],
                             param_fig[0]["height"],
                             ticker,
                             ' Daily High Value',
                             False)

    # # --- VISUALIZE ---
    train_data = pd.Series(stock_model.train_data, name="Value (USD)")
    train_data = train_data.to_frame()
    test_data = pd.Series(stock_model.test_data, name="test_vals")
    test_data = test_data.to_frame()
    fig = px.line(train_data, y="Value (USD)", labels={"Actual"}, title="Daily High Values")
    predicted_data = pd.concat(
        [stock_model.confidence_lolim, stock_model.confidence_hilim, stock_model.forecast_data],
        axis=1)
    fig.add_trace(go.Scatter(name="Actual", x=test_data.index, y=test_data.test_vals))
    fig.add_trace(go.Scatter(name="Predicted", x=predicted_data.index, y=predicted_data.forecast_vals))
    fig.add_trace(go.Scatter(name="95% Conf Lim High", x=predicted_data.index, y=predicted_data.conf_hilim,
                             line=dict(color='cadetblue', width=4,
                                       dash='dash'))
                  )
    fig.add_trace(go.Scatter(name="95% Conf Lim Low", x=predicted_data.index, y=predicted_data.conf_lolim,
                             line=dict(color='cadetblue', width=4,
                                       dash='dash'))
                  )

    # Plotly figure object
    price_chart_figure = fig
    fig_diag = plt.figure(figsize=(param_fig[0]["width_larger_plots"],
                                   param_fig[0]["height_larger_plots"]))
    stock_model.model_auto.plot_diagnostics(fig=fig_diag)
    # plt.show(block=False)
    plt.close('all')
    diagnostic_chart_figure = stock_model.plotly_diagnostic(fig_diag)

    return price_chart_figure, diagnostic_chart_figure
