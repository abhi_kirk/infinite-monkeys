import dash_core_components as dcc
import dash_html_components as html

from pages.forecast.forecast_data import all_tickers, default_stock, data


layout = \
    html.Div(
        children=[
            # Header
            html.Div(
                children=[
                    html.P(children="📈", className="header-emoji"),
                    html.H1(children="Market Predictor", className="header-title",
                            ),
                    html.P(
                        id="header",
                        children="",
                        className="header-description",
                    ),
                    dcc.Interval(
                        id='interval-component',
                        interval=15*1000,
                        n_intervals=0
                    )
                ],
                className="header",
            ),
            # Stock Selector, Dates, Plots
            html.Div(
                children=[
                    # Stock Selector
                    html.Div(
                        children=[
                            html.Div(children="Ticker", className="menu-title"),
                            dcc.Dropdown(
                                id="ticker-filter",
                                options=[
                                    {"label": ticker, "value": ticker}
                                    for ticker in all_tickers
                                ],
                                value=default_stock,
                                clearable=False,
                                className="dropdown",
                            ),
                        ]
                    ),
                    # Calendar
                    html.Div(
                        children=[
                            html.Div(children="Date Range", className="menu-title"),
                            dcc.DatePickerRange(
                                id="date-range",
                                min_date_allowed=data.Value.axes[0].min().date(),
                                max_date_allowed=data.Value.axes[0].max().date(),
                                start_date=data.Value.axes[0].min().date(),
                                end_date=data.Value.axes[0].max().date(),
                            ),
                        ]
                    ),
                ],
                className="menu",
            ),
            # Plots
            html.Div(
                children=[
                    # Plots: Daily High Values
                    html.Div(
                        children=dcc.Graph(
                            id="price-chart",
                            config={"displayModeBar": False},
                        ),
                        className="card",
                    ),
                    # Plots: Daily Close Values
                    html.Div(
                        children=dcc.Graph(
                            id="diagnostic-chart",
                            config={"displayModeBar": False},
                        ),
                        className="card",
                    ),
                ],
                className="wrapper",
            ),
        ]
    )
