from utils.backend.init import INIT
from utils.backend.etl import ETL
from datetime import datetime, timedelta


# --- INITIALIZE ---
c_ = INIT('utils/backend/config.json')
# Read config file
c_.read_json()
param_stock = c_.config_file_data['stock_details']
stock_master_list = c_.config_file_data['ticker_master_list']

# --- ETL ---
# Pull stock data: from start_date until yesterday
curr_stock = ETL(param_stock[0]["stock_name"],
                 c_.config_file_data['dynamo_db'][0])
curr_stock.stock_data(param_stock[0]["start_date"],
                      (datetime.today() - timedelta(days=1)).strftime("%Y-%m-%d"))

data = curr_stock.old
default_stock = curr_stock.ticker
all_tickers = set(stock_master_list[0]['all_tickers'])
