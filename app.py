import dash
import dash_bootstrap_components as dbc
from flask_caching import Cache
import flask
from layout.layout import layout


server = flask.Flask(__name__)

app = dash.Dash(
    __name__,
    server=server,
    title="Infinite-Monkeys",
    suppress_callback_exceptions=True,
    external_stylesheets=[
        dbc.themes.BOOTSTRAP,
        {
            "href": "https://fonts.googleapis.com/css2?"
                    "family=Lato:wght@400;700&display=swap",
            "rel": "stylesheet",
        },
    ],
    meta_tags=[
        {
            "name": "authors",
            "content": "Zeeshan Rizvi, Abhi Jain"
        },
        {
            "name": "viewport",
            "content": "width=device-width, initial-scale=1"
        }
    ]
)

cache = Cache(app.server, config={
    'CACHE_TYPE': 'filesystem',
    'CACHE_DIR': 'cache-directory'
})

app.layout = layout

server = app.server
