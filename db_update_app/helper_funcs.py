import boto3
from boto3.dynamodb.conditions import Key
import json
from decimal import Decimal
from datetime import datetime, date
import csv
import yfinance as yf
import decimal


# Helper class to convert a DynamoDB item to JSON.
class DecimalEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, decimal.Decimal):
            if o % 1 > 0:
                return float(o)
            else:
                return int(o)
        return super(DecimalEncoder, self).default(o)


class DataTable:
    def __init__(self, primary_key, sort_key, value_col, stock_features, run_details):
        self.table = None
        self.tableName = None
        if run_details['run_mode'] == 2:
            self.db = boto3.resource('dynamodb',
                                     aws_access_key_id=run_details['aws_access_key_id'],
                                     aws_secret_access_key=run_details['aws_secret_access_key'])
            self.db_client = boto3.client('dynamodb',
                                          aws_access_key_id=run_details['aws_access_key_id'],
                                          aws_secret_access_key=run_details['aws_secret_access_key'])
        else:
            self.db = boto3.resource('dynamodb',
                                     endpoint_url=run_details['endpoint_url'])
            self.db_client = boto3.client('dynamodb',
                                          endpoint_url=run_details['endpoint_url'])
        self.primary_key = primary_key
        self.sort_key = sort_key
        self.value_col = value_col
        self.features = stock_features
        self.timezone = 'US/Eastern'
        self.stock_period = "1d"
        self.stock_interval = "1d"
        self.todays_date = date.today().strftime("%Y-%m-%d")
        print("Initialized")

    def createTable(self, tableName, KeySchema, AttributeDefinitions, ProvisionedThroughput):
        self.tableName = tableName
        table = self.db.create_table(
            TableName=tableName,
            KeySchema=KeySchema,
            AttributeDefinitions=AttributeDefinitions,
            ProvisionedThroughput=ProvisionedThroughput
        )
        self.table = table
        print(f'Created Table {self.table}')

    @staticmethod
    def convert_unix_to_readable_gmt(unix_ts):
        return datetime.utcfromtimestamp(int(unix_ts)).strftime('%Y-%m-%d')

    def check_table_empty(self):
        # Assumption: Non-empty table will always have GOOGLE Closing Price data
        result = self.table.query(KeyConditionExpression=Key(self.primary_key).eq('GOOG-Close'))
        if result['Items']:
            return False
        else:
            return True

    def get_last_ts(self, stock_list):
        stock_last_ts = []
        for stock in stock_list:
            result = self.table.query(KeyConditionExpression=Key(self.primary_key).eq(f'{stock}-Close'))
            last_ts = [int(i[self.sort_key]) for i in result['Items']][-1]
            stock_last_ts.append(self.convert_unix_to_readable_gmt(last_ts))
        return {stock_list[i]: stock_last_ts[i] for i in range(len(stock_list))}

    def insert_data(self, json_path=None, json_obj=None):
        if json_path:
            with open(json_path) as f:
                data = json.load(f, parse_float=Decimal)
        elif json_obj:
            data = json_obj
        else:
            return Exception('No JSON filepath or obj found')
        for item in data:
            try:
                self.table.put_item(Item=item)
            except Exception as e:
                print(f'Error inserting data: {e}')
                pass
        print(f'Inserted Data into {self.tableName}')

    def getItem(self, key):
        try:
            response = self.table.get_item(Key=key)
            return response['Item']
        except Exception as e:
            print(f'Item not found: {e}')
            return None

    def create_data_csv(self, query=None, filename="data.csv"):
        if query:
            data = self.table.scan(FilterExpression=query)
        else:
            data = self.table.scan()
        if len(data['Items']) != 0:
            items = data['Items']
            keys = items[0].keys()
            with open(filename, 'w') as f:
                for i in items:
                    dict_writer = csv.DictWriter(f, keys)
                    if f.tell() == 0:
                        dict_writer.writeheader()
                        dict_writer.writerow(i)
                    else:
                        dict_writer.writerow(i)
            f.close()

    def create_json_dump(self, stock_list, stock_start_dates):
        fields = [self.primary_key, self.sort_key, self.value_col]
        features = self.features
        json_stock_data = []
        for stock in stock_list:
            obj = yf.Ticker(stock)
            data = obj.history(start=stock_start_dates[stock],
                               end=self.todays_date,
                               interval=self.stock_interval)
            data['UNIX_TS'] = data.index.astype(int) // 10 ** 9
            data['Date'] = data.index.astype(str)
            for _, row in data.iterrows():
                for feat in features:
                    fields_val = [
                        str(stock + '-' + feat),
                        int(row['UNIX_TS']),
                        round(Decimal(row[feat]), 2)
                    ]
                    json_stock_data.append({fields[i]: fields_val[i] for i in range(len(fields))})
        return json_stock_data
