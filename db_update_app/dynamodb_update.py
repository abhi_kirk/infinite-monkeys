from helper_funcs import DataTable
import schedule
import time
from datetime import datetime
import os

os.environ['AWS_DEFAULT_REGION'] = 'us-east-2'
# os.environ['AWS_ACCESS_KEY_ID'] = 'AKIARLBSTVQYA63XSEHM'
# os.environ['AWS_SECRET_ACCESS_KEY'] = '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'


class DynamodbUpdate:
    def __init__(self, run_info, stock_list_, default_start_date_, table_name_):
        self.stock_list = stock_list_
        self.default_start_date = default_start_date_
        self.table_name = table_name_
        self.stock_table = DataTable(primary_key='Ticker-Feature',
                                     sort_key='TimeStamp',
                                     value_col='Value',
                                     stock_features=['Close', 'Volume'],
                                     run_details=run_info)

    def health_check(self):
        describe = self.stock_table.db_client.describe_table(TableName=self.table_name)
        health = describe['Table']['TableStatus']  # CREATING | UPDATING | DELETING | ACTIVE | INACCESSIBLE
        print(f"DB Heartbeat: {health}")
        if not os.path.isdir("appdata"):
            os.mkdir("appdata")
        health_file = open(os.path.join("appdata", "hbeat.txt"), "a")
        health_file.writelines(f"\n{datetime.now().strftime('%Y-%m-%d %H:%M:%S')}-- DB Heartbeat: {health}\n")
        health_file.close()
        return health

    def main(self):
        print(f'Starting DynamoDB Update Job at {datetime.now().strftime("%Y-%m-%d %H:%M:%S")}')
        try:
            key_schema = [
                {
                    'AttributeName': self.stock_table.primary_key,
                    'KeyType': 'HASH'
                },
                {
                    'AttributeName': self.stock_table.sort_key,
                    'KeyType': 'RANGE'
                }
            ]
            attribute_datatype = [
                {
                    'AttributeName': self.stock_table.primary_key,
                    'AttributeType': 'S'
                },
                {
                    'AttributeName': self.stock_table.sort_key,
                    'AttributeType': 'N'
                }
            ]
            provisioned_throughput = {
                'ReadCapacityUnits': 10,
                'WriteCapacityUnits': 10
            }
            self.stock_table.createTable(
                tableName=table_name,
                KeySchema=key_schema,
                AttributeDefinitions=attribute_datatype,
                ProvisionedThroughput=provisioned_throughput
            )
            time.sleep(5)
        except Exception as e:
            if e.__class__.__name__ == "ResourceInUseException":
                print(f"Table {table_name} already exists")
                self.stock_table.table = self.stock_table.db.Table(table_name)
                pass

        if self.stock_table.check_table_empty():
            query_start_dates = {stock_list[i]: default_start_date for i in range(len(stock_list))}
        else:
            query_start_dates = self.stock_table.get_last_ts(stock_list)
        table_input = self.stock_table.create_json_dump(stock_list=stock_list, stock_start_dates=query_start_dates)
        self.stock_table.insert_data(json_obj=table_input)
        self.stock_table.create_data_csv()


if __name__ == "__main__":
    run_details = {}
    if not os.getenv("AWS_SECRET_ACCESS_KEY"):
        print("Running on localhost...\n")
        run_details["run_mode"] = 1
        run_details["endpoint_url"] = input("Enter Local DynamoDB Host End-Point URL: ")
    else:
        print("Running on AWS...")
        run_details["run_mode"] = 2
        run_details['aws_access_key_id'] = os.getenv("AWS_ACCESS_KEY_ID")
        run_details['aws_secret_access_key'] = os.getenv("AWS_SECRET_ACCESS_KEY")

    stock_list = ['GOOG', 'AAPL', 'MSFT', 'NVDA']
    default_start_date = "2021-01-01"
    table_name = 'stock-features'
    obj = DynamodbUpdate(run_details, stock_list, default_start_date, table_name)
    obj.main()
    schedule.every().day.at("07:00").do(obj.main)
    schedule.every(15).seconds.do(obj.health_check)
    while True:
        schedule.run_pending()
        time.sleep(5)
