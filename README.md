# Infinite Monkeys
Stock Market Prediction with Data Science Tools

## Objective
Break the market

## Summary
- Stock market prediction app using pmdARImA timeseries modeling 
- Dashboard created using Dash and deployed with AWS Beanstalk
- [Jira](https://hogwarts-alumni.atlassian.net/jira/software/projects/MONKEYS/)
- Two applications are developed:
  - *Flask frontend+backend app*: For computation and processing for forecasting. Queries data from a DynamoDB instance on AWS. 
  - *DynamoDB update/maintenance app*: For periodically inserting stocks-related data into a DynamoDB instance, using the yfinance library. 

## Tech Specifications
- python 3.8.5

## User Interface
![Text Gen](imgs/dashboard.png)

## Real Objectives
- Use Agile approach using Git, AWS, Jira
- Build timeseries model to predict stock trends
- Create an interactive dashboard
- Integrate both price and sentiment features for predictions
- Studying and implementing various ML algorithms 
- JS React as backend for dashboard
- Open-source for community support

## Deployment Instructions
- When running locally add `ENVIRONMENT_FILE: .env.development` as an environment variable
  - E.g. With PyCharm:
![Text Gen](imgs/env_add.png)
- Docker:
  - [Install Docker Desktop](https://docs.docker.com/get-docker/)
    - Check: `docker run hello-world`
  - If building from source: `docker build -t username/image_name:X.Y .`, where:
      - `username` is the [Docker Hub](https://hub.docker.com/) username, 
      - `image_name` is the desired image name to be published, 
      - `X.Y` is the image version number. 
  - If retrieving built image: `docker pull username/image_name:X.Y`
  - Run containerized app: `docker run --name container_name -it -p 8888:8085 username/image_name:X.Y`
    - `-it` is for interactive terminal so debug runtime logs are visible,
    - `-p 8888:8085` is for docker container port (8085) mapped to host (your PC) port (8888)
  - Alternatively: `docker-compose up -d`
    - `-d` is for detached mode
  - Dashboard: in a browser go to `http://0.0.0.0:8888/` and click on _Stock Forecast_ from the left sidebar.
  - Defaults:
    - username: `hogwartsalumni`, 
    - image_name: 
      - Flask app: `inf-monkeys-main`,
      - DynamoDB Maintenance app: `inf-monkeys-dbupdate`
    - [Docker Hub Repo](https://hub.docker.com/repositories)
