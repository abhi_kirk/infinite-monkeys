import time
from datetime import datetime
from decimal import Decimal
from app import cache
import pandas as pd
import boto3
from boto3.dynamodb.conditions import Key
from utils.constants import TIMEOUT
import os

os.environ['AWS_DEFAULT_REGION'] = 'us-east-2'


class ETL:
    # init method or constructor
    def __init__(self, name, db_config):
        self.ticker = name
        self.old = None
        self.config = db_config
        self.db = None
        self.db_client = None
        self.partition_key = None
        self.sort_key = None
        self.table = None
        self.err_msg = None
        if self.validate_dynamodb():
            print(f"DynamoDB Table {self.table} validated.")
        else:
            Exception(self.err_msg)

    @staticmethod
    def convert_unix_to_readable_gmt(unix_ts):
        return datetime.utcfromtimestamp(int(unix_ts)).strftime('%Y-%m-%d')

    @staticmethod
    def convert_unix_to_gmt(unix_ts):
        return datetime.utcfromtimestamp(int(unix_ts))

    def validate_dynamodb(self, active_timeout=60):
        c = self.config

        # connect to DB
        if c['db_source'] == "local":
            self.db = boto3.resource('dynamodb',
                                     endpoint_url=c['local_host_endpoint_url'])
            self.db_client = boto3.resource('dynamodb',
                                            endpoint_url=c['local_host_endpoint_url'])
        elif c['db_source'] == "aws":
            self.db = boto3.resource('dynamodb',
                                     aws_access_key_id=os.getenv("AWS_ACCESS_KEY_ID"),
                                     aws_secret_access_key=os.getenv("AWS_SECRET_ACCESS_KEY"))
            self.db_client = boto3.client('dynamodb',
                                          aws_access_key_id=os.getenv("AWS_ACCESS_KEY_ID"),
                                          aws_secret_access_key=os.getenv("AWS_SECRET_ACCESS_KEY"))
        else:
            self.err_msg = "CONFIG: Wrong DynamoDB Source Selected."
            return False

        # check table exists
        try:
            table_info = self.db_client.describe_table(TableName=c['table_name'])
            self.partition_key = table_info['Table']['KeySchema'][0]['AttributeName']
            self.sort_key = table_info['Table']['KeySchema'][1]['AttributeName']
            if table_info['Table']['TableStatus'] != "ACTIVE":
                time.sleep(active_timeout)
            if table_info['Table']['TableStatus'] != "ACTIVE":
                self.err_msg = f"Table status is {table_info['Table']['TableStatus']}.. Timed Out ({active_timeout}s)"
                return False
        except:
            self.err_msg = f"Database Table {c['table_name']} does not exist. Please check DB Maintenance App."
            return False

        # check table not empty
        if table_info['Table']['ItemCount'] == 0:
            self.err_msg = f"Table {c['table_name']} is Empty. Please check DB Maintenance App."
            return False
        else:
            self.table = self.db.Table(c['table_name'])
        return True

    @cache.memoize(timeout=TIMEOUT)
    def stock_data(self, date_start, date_end):
        date_start = Decimal(datetime.strptime(date_start, '%Y-%m-%d').timestamp())
        date_end = Decimal(datetime.strptime(date_end, '%Y-%m-%d').timestamp())
        query_result = self.table.query(
            KeyConditionExpression=
            Key(self.partition_key).eq(self.ticker+'-Close') &
            Key(self.sort_key).between(date_start, date_end)
        )
        df = pd.DataFrame(query_result['Items'])
        df.index = df['TimeStamp'].apply(self.convert_unix_to_gmt)
        self.old = df
