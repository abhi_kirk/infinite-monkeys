import pandas as pd
from matplotlib import pyplot as plt
from pmdarima.arima import auto_arima
from statsmodels.tsa.arima.model import ARIMA
from plotly.subplots import make_subplots
import plotly.graph_objects as go
import warnings

warnings.filterwarnings('ignore')


class Model:
    # init method or constructor
    def __init__(self):
        self.rolmean = []
        self.rolstd = []
        self.train_data = []
        self.test_data = []
        self.model_auto = None
        self.model = None
        self.forecast_data = []
        self.confidence_lolim = []
        self.confidence_hilim = []

    def model_arima(self, data, p0, q0, max_p, max_q, m, train_test_split, dim_w, dim_h):
        self.train_data = pd.Series(data[3:int(len(data) * train_test_split)], name="train_vals")
        self.test_data = data[int(len(data) * train_test_split):]
        self.model_auto = auto_arima(self.train_data, start_p=p0, start_q=q0,
                                     test='adf',  # use adftest to find optimal 'd'
                                     max_p=max_p, max_q=max_q,
                                     m=m,  # frequency of series
                                     d=None,  # let model determine 'd'
                                     seasonal=True,  # No Seasonality
                                     start_P=p0,
                                     D=0,
                                     trace=True,
                                     error_action='ignore',
                                     suppress_warnings=True,
                                     stepwise=True)
        print(self.model_auto.summary())

        # Use the model order obtained by the model_auto to build an ARIMA model
        self.model = ARIMA(self.train_data, order=self.model_auto.order)

    def forecast_val(self, alpha, dpi, dim_w, dim_h, ticker, label, ext_figure):
        fitted = self.model.fit()

        # confidence interval (95% if alpha=0.05)
        forecast = fitted.get_forecast(len(self.test_data))
        fc = forecast.predicted_mean
        forecast_conf = forecast.conf_int(alpha=alpha)
        self.forecast_data = pd.Series(fc.values, index=self.test_data.index, name="forecast_vals")
        self.confidence_lolim = pd.Series(forecast_conf['lower train_vals'].values,
                                          index=self.test_data.index, name="conf_lolim")
        self.confidence_hilim = pd.Series(forecast_conf['upper train_vals'].values,
                                          index=self.test_data.index, name="conf_hilim")

        # Plot if external figure flag is True
        if ext_figure:
            plt.figure(figsize=(dim_w, dim_h), dpi=dpi)
            plt.plot(self.train_data, label='Training Data')
            plt.plot(self.test_data, color='blue', label='Actual Stock Price')
            plt.plot(self.forecast_data, color='orange', label='Predicted Stock Price')
            plt.fill_between(self.confidence_lolim.index, self.confidence_lolim, self.confidence_hilim,
                             color='k', alpha=.10)
            plt.title(ticker + label)
            plt.xlabel('Time')
            plt.ylabel('USD')
            plt.legend()
            plt.show()

    @staticmethod
    def plotly_diagnostic(fig_diag):
        # Extract diagnostic plot from plt and return plotly object

        # Extract subplot data from plt
        diag_sp = fig_diag.get_children()
        sp_resid = diag_sp[1]
        resid_x = sp_resid.lines[0].get_xdata()
        resid_y = sp_resid.lines[0].get_ydata()

        sp_hist = diag_sp[2]
        hist_kde_x = sp_hist.lines[0].get_xdata()
        hist_kde_y = sp_hist.lines[0].get_ydata()
        hist_n_x = sp_hist.lines[1].get_xdata()
        hist_n_y = sp_hist.lines[1].get_ydata()
        hist_bars = sp_hist.patches

        sp_qq = diag_sp[3]
        qq_sc_x = sp_qq.lines[0].get_xdata()
        qq_sc_y = sp_qq.lines[0].get_ydata()
        qq_line_x = sp_qq.lines[1].get_xdata()
        qq_line_y = sp_qq.lines[1].get_ydata()

        sp_corr = diag_sp[4]
        corr_bar = sp_corr.lines[0].get_xdata()
        corr_bar = sp_corr.lines[0].get_ydata()
        corr_x = sp_corr.lines[1].get_xdata()
        corr_y = sp_corr.lines[1].get_ydata()

        fig_plotly = make_subplots(rows=2, cols=2,
                                   subplot_titles=(
                                       'Standardized Residual', 'Histogram and Estimated Density', 'Normal Q-Q',
                                       'Correlogram'))

        # Subplot: Residuals
        fig_plotly.add_trace(go.Scatter(name='Model Residuals', x=resid_x, y=resid_y, showlegend=False),
                             row=1, col=1)

        # Subplot: Histogram
        fig_plotly.add_trace(go.Scatter(name='KDE', x=hist_kde_x, y=hist_kde_y), row=1, col=2)
        fig_plotly.add_trace(go.Scatter(name='N(0,1)', x=hist_n_x, y=hist_n_y), row=1, col=2)

        # Subplot: Q-Q Plot
        fig_plotly.add_trace(go.Scatter(x=qq_sc_x, y=qq_sc_y, mode='markers', showlegend=False,
                                        marker=dict(color='LightSkyBlue',
                                                    line=dict(color='MediumPurple', width=2))), row=2, col=1)
        fig_plotly.add_trace(go.Scatter(x=qq_line_x, y=qq_line_y, showlegend=False,
                                        line=dict(color='red', width=4,
                                                  dash='dash')), row=2, col=1)

        fig_plotly.update_xaxes(title_text="Theoretical Quantiles", row=2, col=1)
        fig_plotly.update_yaxes(title_text="Sample Quantiles", row=2, col=1)

        # Subplot: Correlogram
        fig_plotly.add_trace(go.Bar(x=corr_x, y=corr_y, showlegend=False), row=2, col=2)
        fig_plotly.update_layout(title_text="Time Series Model Diagnostics")

        # fig_plotly.write_image("diagnotic_plots.png")
        return fig_plotly
