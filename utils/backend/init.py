import json


# Init class constructor
class INIT:
    # init method or constructor
    def __init__(self, file_name):
        print("Loading JSON config file with stock metadata")
        # Read JSON config file
        f = open(file_name, )
        self.config_file_obj = f
        self.config_file_data = []

    def read_json(self):
        # Output content of JSON config file as a dictionary
        self.config_file_data = json.load(self.config_file_obj)