import pandas as pd
from scipy import signal as scipy
from matplotlib import pyplot as plt
from statsmodels.tsa.seasonal import seasonal_decompose


class Asys:
    # init method or constructor
    def __init__(self):
        self.filtered = []
        self.rolmean = []
        self.rolstd = []
        self.data_decomp = []

    # SavGol filter method using a moving-window size 'win_size' and polynomal order 'order'
    def filter(self, val, win_size, order):
        self.filtered = scipy.savgol_filter(val, win_size, order)
        self.filtered = pd.Series(self.filtered, index=val.index)

    def plot_raw_filtered(self, val, dim_w, dim_l, ticker, label):
        plt.figure(figsize=(dim_w, dim_l))
        plt.plot(val, label=ticker)
        plt.plot(val.axes[0].T, self.filtered, '--', label=ticker + ' filt')
        plt.title(ticker + label)
        plt.legend()
        plt.ylabel('USD')
        plt.xlabel('Time')
        plt.show()

    # Check if timeries data is stationary
    def check_stationarity(self, val, win_size, dim_w, dim_h, dim_large_w, dim_large_h, ticker, label):
        series = pd.Series(val)
        self.rolmean = series.rolling(win_size).mean()
        self.rolstd = series.rolling(win_size).std()
        plt.figure(figsize=(dim_w, dim_h))
        plt.plot(series, label='Raw Value')
        plt.plot(self.rolmean, '--', label='Moving Avg.')
        plt.plot(self.rolstd, '.-', label='Moving Std')
        plt.legend()
        plt.title(ticker + label)
        plt.ylabel('USD')
        plt.xlabel('Time')
        plt.show()

        # Decomposing time-series data in seasonality, trend, and base values (to be tested later)
        self.data_decomp = seasonal_decompose(series, model='multiplicative', freq=5)
        fig = self.data_decomp.plot()
        fig.set_size_inches(dim_large_w, dim_large_h)
