import pandas as pd
from scipy.signal import savgol_filter
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import mean_squared_error
import pickle
import os
from datetime import datetime


class TrainingML:
    def __init__(self, config, db):
        self.c = config
        self.db = db
        self.tickers = []
        self.feats = []
        self.models = {}
        if not os.path.isdir("models"):
            os.mkdir("models")
        self.tnow = datetime.now().strftime("%Y-%m-%d %H-%M")

    def model_output_format(self):
        for ticker in self.tickers:
            self.models[ticker] = {
                'algo': None,
                'path': None,
                'mse': None,
                'hyperparams': None,
                'feat_importance': None,
                'tstamp': None
            }

    def data_formatting(self, json_dump):
        # cols: tstamp, ticker, feat1, feat2, ...
        df = pd.DataFrame.from_records(json_dump)
        df['TimeStamp'] = df['TimeStamp'].apply(self.db.convert_unix_to_readable_gmt)
        df[['Ticker', 'Feature']] = df['Ticker-Feature'].str.split('-', expand=True)
        df.drop('Ticker-Feature', axis=1, inplace=True)
        self.tickers = df['Ticker'].unique().tolist()
        self.model_output_format()
        self.feats = df['Feature'].unique().tolist()
        df_processed = []
        for ticker in self.tickers:
            df_ticker = df[df['Ticker'] == ticker]
            df_ticker_processed = pd.DataFrame()
            for feat in self.feats:
                df_ticker_processed[feat] = df_ticker[df_ticker['Feature'] == feat]['Value'].values
                df_ticker_processed['TimeStamp'] = df_ticker[df_ticker['Feature'] == feat]['TimeStamp'].values
            df_ticker_processed['Ticker'] = ticker
            df_processed.append(df_ticker_processed)
        return pd.concat(df_processed, axis=0).reset_index().set_index(['Ticker'])

    def data_split(self, df):
        split_perc = self.c['analysis_model_param'][0]['data_split_train_perc']
        df_split = []
        for ticker in self.tickers:
            df_ticker = df.loc[ticker].copy(deep=True)
            df_ticker['Split'] = ""
            n = round(len(df_ticker) * split_perc)
            df_ticker.loc[df_ticker['index'] <= n, 'Split'] = 'train'
            df_ticker.loc[df_ticker['index'] > n, 'Split'] = 'test'
            df_split.append(df_ticker)
        return pd.concat(df_split, axis=0)

    def preprocess(self, df):
        savgol_config = self.c['filter_param'][0]
        df_preprocess = []
        for ticker in self.tickers:
            df_ticker = df.loc[ticker].copy(deep=True)
            for feat in self.feats:
                feat_new = feat+'-Filter'
                df_ticker[feat_new] = savgol_filter(
                    df_ticker[feat].values,
                    savgol_config['win_size'],
                    savgol_config['poly_order']
                )
                df_ticker[feat_new] = df_ticker[feat_new].astype(float)
            df_preprocess.append(df_ticker)
        return pd.concat(df_preprocess, axis=0)

    def train_models(self, df):
        df_modeling = []
        for ticker in self.tickers:
            df_ticker = df.loc[ticker].copy(deep=True)
            X_train = df_ticker.loc[df_ticker['Split'] == 'train', [i+'-Filter' for i in self.feats]]
            y_train = df_ticker.loc[df_ticker['Split'] == 'train', 'Close']
            rf = RandomForestRegressor()
            rf.fit(X_train, y_train)
            self.models[ticker]['algo'] = str(rf.base_estimator)
            self.models[ticker]['feat_importance'] = str(rf.feature_importances_.tolist())
            self.models[ticker]['hyperparams'] = rf.get_params()
            self.save_model(ticker, rf)

            X_test = df_ticker.loc[df_ticker['Split'] == 'test', [i+'-Filter' for i in self.feats]]
            df_ticker['Close-Pred'] = rf.predict(X_train).tolist() + rf.predict(X_test).tolist()
            df_modeling.append(df_ticker)
        return pd.concat(df_modeling, axis=0)

    def eval_models(self, df):
        ticker_eval = {}
        for ticker in self.tickers:
            df_ticker = df.loc[ticker].copy(deep=True)
            y_test = df_ticker.loc[df_ticker['Split'] == 'test', 'Close']
            y_pred = df_ticker.loc[df_ticker['Split'] == 'test', 'Close-Pred']
            mse = mean_squared_error(y_test, y_pred)
            ticker_eval[ticker] = mse
            self.models[ticker]['mse'] = mse
        return ticker_eval

    def save_model(self, ticker, model):
        model_path = os.path.join("models", ticker+'-'+self.tnow+'.sav')
        pickle.dump(model, open(model_path, 'wb'))
        self.models[ticker]['path'] = model_path
        self.models[ticker]['tstamp'] = self.tnow
