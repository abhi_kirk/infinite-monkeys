import json
from training_helper import TrainingML
from db_update_app.helper_funcs import DataTable
import os


def read_config(config_path):
    file = open(config_path)
    c = json.load(file)
    return c


def get_run_details():
    run_details = {'run_mode': 2,
                   'aws_access_key_id': 'AKIARLBSTVQYA63XSEHM',
                   'aws_secret_access_key': '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'}
    return run_details


config = read_config(os.path.join(os.getcwd(), "../utils/backend", "config.json"))

# connect to DB and retrieve training/testing data
db_obj = DataTable(primary_key='Ticker-Feature',
                   sort_key='TimeStamp',
                   value_col='Value',
                   stock_features=['Close', 'Volume'],
                   run_details=get_run_details())
db_obj.table = db_obj.db.Table('stock-features')
if db_obj.check_table_empty():
    print("DynamoDB table is empty.. exiting.")
    exit()

data_json = db_obj.table.scan()

# preprocess data
t = TrainingML(config, db_obj)
data = t.data_formatting(data_json['Items'])
data = t.data_split(data)
data = t.preprocess(data)

# train ML model
data = t.train_models(data)

# test ML model, display metrics
print(f"Mean-Squared Error: {t.eval_models(data)}")

# save model and metadata
with open(f"results-{t.tnow}.json", "w") as f:
    json.dump(t.models, f)
