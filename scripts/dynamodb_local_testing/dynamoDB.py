from dynamoDB_helper import DataTable


def main():
    stock_list = ['GOOG', 'AAPL', 'MSFT', 'JPM']
    default_start_date = "2022-01-01"
    stock_features = DataTable(primary_key='Ticker-Feature',
                               sort_key='TimeStamp',
                               value_col='Value',
                               stock_features=['Close', 'Volume'])
    table_name = 'stock-features'
    try:
        key_schema = [
            {
                'AttributeName': stock_features.primary_key,
                'KeyType': 'HASH'
            },
            {
                'AttributeName': stock_features.sort_key,
                'KeyType': 'RANGE'
            }
        ]
        attribute_datatype = [
            {
                'AttributeName': stock_features.primary_key,
                'AttributeType': 'S'
            },
            {
                'AttributeName': stock_features.sort_key,
                'AttributeType': 'N'
            }
        ]
        provisioned_throughput = {
            'ReadCapacityUnits': 10,
            'WriteCapacityUnits': 10
        }
        stock_features.createTable(
            tableName=table_name,
            KeySchema=key_schema,
            AttributeDefinitions=attribute_datatype,
            ProvisionedThroughput=provisioned_throughput
        )
    except Exception as e:
        if e.__class__.__name__ == "ResourceInUseException":
            print(f"Table {table_name} already exists")
            stock_features.table = stock_features.db.Table(table_name)
            pass

    breakpoint()
    if stock_features.check_table_empty():
        query_start_dates = {stock_list[i]: default_start_date for i in range(len(stock_list))}
    else:
        query_start_dates = stock_features.get_last_ts(stock_list)
    table_input = stock_features.create_json_dump(stock_list=stock_list, stock_start_dates=query_start_dates)
    stock_features.insert_data(json_obj=table_input)
    stock_features.create_data_csv()


if __name__ == "__main__":
    main()
